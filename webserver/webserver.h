/***************************************************************************
                           webserver.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef WEBSERVER_H
#define WEBSERVER_H

#include "utils/threadinterface.h"
#include <mongoose/WebController.h>

// forward declaration
class DeviceAdministrationItf;

class Webserver 
: public ThreadInterface
{
public:
  Webserver(DeviceAdministrationItf* pAdmin, std::string& documentRoot);
  ~Webserver();
  DeviceAdministrationItf* getAdminItf();
protected:
  void run() override;
protected:
  DeviceAdministrationItf* m_pAdmin;
  std::string m_documentRoot;
};

#endif // WEBSERVER_H
