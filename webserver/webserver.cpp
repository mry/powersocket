/***************************************************************************
                           webserver.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "webserver.h"

#include <unistd.h>
#include <mongoose/Server.h>
#include <mongoose/WebController.h>
#include <digitalSTROM/dsuid.h>

#include "utils/deviceadministrationitf.h"
#include "utils/converter.h"

#include<iostream>
#include<string>

using namespace Mongoose;
using namespace std;

/***********************************************************************//**
  @method : Webserver
  @comment: constructor
  @param  : pAdmin DeviceAdministrationItf
  @param  : documentRoot base path for document root.
            The html pages are under /www subdirectory
***************************************************************************/
Webserver::Webserver(DeviceAdministrationItf* pAdmin, std::string& documentRoot) 
: m_pAdmin{pAdmin}
, m_documentRoot{documentRoot}
{
}

/***********************************************************************//**
  @method :  ~Webserver
  @comment:  destructor
***************************************************************************/
Webserver::~Webserver()
{
}

/***********************************************************************//**
  @method :  getAdminItf
  @comment:  get the administration interface
  @return :  DeviceAdministrationItf pointer
***************************************************************************/
DeviceAdministrationItf* Webserver::getAdminItf()
{
  return m_pAdmin;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void Webserver::run()
{
  Server server(8090, m_documentRoot.c_str()); // port 8090
  server.start();
  while(isRunning())
  {
    sleep(1);
  }
  server.stop();
}
