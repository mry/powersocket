/***************************************************************************
                           VDSDDigOutput.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ivdcaccess.h"
#include "utils/logger.h"
#include "vdsdDigOutput.h"

#include "utils/interfacehelper.h"

#include <string.h>
#include <stdlib.h>

/***********************************************************************//**
  @comment:  static initializer
***************************************************************************/
uint8_t* VDSDDigOutput::m_icon = nullptr;
size_t VDSDDigOutput::m_iconSize = 0;
std::string VDSDDigOutput::m_iconName = "default";

/***********************************************************************//**
  @method :  VDSDDigOutput
  @comment:  default constructor
***************************************************************************/
VDSDDigOutput::VDSDDigOutput() :
  m_apply{false},
  m_tempValue{0.0},
  m_scene{0},
  m_pin{0},
  m_name{"output"},
  m_function{0},
  m_group{8}, // joker
  m_mode{1} // switch on/off
{
  initialize();
}

/***********************************************************************//**
  @method :  VDSDDigOutput
  @comment:  constructor
***************************************************************************/
VDSDDigOutput::VDSDDigOutput(int32_t pin) 
: m_apply{false}
, m_tempValue{0.0}
, m_scene{0}
, m_pin{pin}
, m_name{"output"}
, m_function{0}
, m_group{8} // joker
, m_mode{1}  // switch on/off
{
  initialize();
}

/***********************************************************************//**
  @method :  initialize
  @comment:  called from constructors
***************************************************************************/
void VDSDDigOutput::initialize()
{
  for (int i=0; i<MAX_SCENES; ++i) {
    m_value[i] = 0.0;
  }

  m_value[13] = 0.0;   // min scene
  m_value[14] = 100.0; // max scene
  m_value[40] = 0.0;   // Fade Off

  for (int i = 0; i < MAX_GROUPS; ++i) {
      m_groupMember[i] = false;
  }
  m_tempValue = 0.0;
}

/***********************************************************************//**
  @method :  ~VDSDDigOutput
  @comment:  destructor
***************************************************************************/
VDSDDigOutput::~VDSDDigOutput()
{
}


/***********************************************************************//**
  @method :  setGlobalIcon
  @comment:  set Icon of device type VDSDDigOutput
             image must be of type png 16
  @param  :  icon16png icon
  @param  :  size size of the icon
  @param  :  iconName name
***************************************************************************/
void VDSDDigOutput::setGlobalIcon(const uint8_t* icon16png, size_t size, std::string &iconName)
{
  if (m_icon) {
    free (m_icon);
    m_icon = NULL;
  }
  m_icon = (uint8_t*)malloc(size);
  memcpy(m_icon, icon16png, size);
  m_iconSize = size;
  m_iconName = iconName;
}

/***********************************************************************//**
  @method :  getValue
  @comment:  getter
  @param  :  channel index of array
  @return :  channel value
***************************************************************************/
double VDSDDigOutput::getValue(int channel) const
{
  if (channel == 0) {
    return m_tempValue;
  }
  XLOG_ERR("getValue:channel out of range: " << channel);
  return -1;
}

/***********************************************************************//**
  @method :  getApply
  @comment:  getter
  @return :  apply value
***************************************************************************/
bool VDSDDigOutput::getApply() const
{
  return m_apply;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::process()
{
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleSetControl(int32_t value, int32_t group, int32_t zone)
{
  // TODO
  XLOG_INFO("setControl:value: " << value 
  << ", group: " << group
  << ", zone:" << zone);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleSetChannelValue(bool apply, double value, int32_t channel)
{
  // TODO
  XLOG_INFO("handleSetChannelValue:apply: " << apply
           << ", value: " << value
           << ", channel: " << channel);
  if (0 == channel) {
    m_tempValue = value;
  }
  m_apply = apply;
  if (m_apply) {
    emitValue(m_tempValue);
  }
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleSetCallScene(int32_t scene, bool force, int32_t group, int32_t zone)
{
  m_scene = scene;
  m_tempValue  = m_value[m_scene];
  XLOG_DEBUG("handleSetCallScene: scene: " << scene
            << ", force: " << force 
            << ", group: " << group
            << ", zone: " << zone 
            << ", value: " << m_tempValue);
  emitValue(m_tempValue);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleSetSaveScene(int32_t scene, int32_t group, int32_t zone)
{
  m_scene = scene;
  m_value[m_scene] = m_tempValue;
  XLOG_DEBUG("handleSetSaveScene: scene: " << scene 
            << ", group: " << group
            << ", zone: "<< zone 
            << ", value: " << m_value[m_scene]);
  emitValue(m_tempValue);
  notifyModelChange();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleSetMinScene(int32_t group, int32_t zone)
{
  m_scene = 0;
  m_tempValue = m_value[m_scene];
  XLOG_DEBUG("handleSetMinScene: group: " << group
            <<", zone: " << zone 
            << ", value: " << m_tempValue);
  emitValue(m_tempValue);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleIdentify(int32_t /*group*/, int32_t /*zone_id*/)
{
  emitValue(0);
  emitValue(100);
  emitValue(0);
  emitValue(100);
  emitValue(m_tempValue);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetPrimaryGroup(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_uint(property, name, 8); // joker
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetModelFeatures(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  //(void)query; (void) name;

  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    XLOG_WARNING("failed to allocate reply property for " << name);
    return;
  }

  dsvdc_property_add_bool(reply, "blink" ,      true);
  dsvdc_property_add_bool(reply, "dontcare",    true);
  dsvdc_property_add_bool(reply, "outmode",     true);
  dsvdc_property_add_bool(reply, "outvalue8",   true);
  dsvdc_property_add_bool(reply, "transt",      true);
  dsvdc_property_add_bool(reply, "jokerconfig", true);

  dsvdc_property_add_property(property, name, &reply);
};

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetVersionGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "1.0.1");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetHardwareVersion(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "1.0.0");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetConfigURL(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  static const size_t LEN = 20;
  char data[LEN];
  InterfaceHelper::GetPrimaryIp(data, LEN);
  std::string output = "http://" + std::string(data) + ":8090";
  dsvdc_property_add_string(property, name, output.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetHardwareGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  std::string guid = "guid:";
  dsuid_t dsuid = getDsuid();
  char data[DSUID_STR_LEN];
  dsuid_to_string(&dsuid,data);
  guid += data;
  dsvdc_property_add_string(property, name, guid.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetHardwareModelGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  std::string guid = "powersocket:";
  dsuid_t dsuid = getDsuid();
  char data[DSUID_STR_LEN];
  dsuid_to_string(&dsuid,data);
  guid += data;
  dsvdc_property_add_string(property, name, guid.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetModelUID(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsuid_t dsuid = getDsuid();
  char data[DSUID_STR_LEN];
  dsuid_to_string(&dsuid, data);
  dsvdc_property_add_string(property, name, data); // TODO
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetName(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, m_name.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetModel(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  std::string info("Output");
  dsvdc_property_add_string(property, name, info.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetModelGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  //char dsuidstringVdcd[DSUID_STR_LEN];
  //dsuid_t temp = getDsuid();
  //::dsuid_to_string(&temp, dsuidstringVdcd);

  //char info[256];
  //strcpy(info, "dsuid:");
  //strcat(info, dsuidstringVdcd);
  
  std::string identifier {"powersocket:"};
  identifier += std::to_string(m_pin);
  dsvdc_property_add_string(property, name, identifier.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetOutputDescription(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    XLOG_WARNING("failed to allocate reply property for " << name);
    return;
  }

  dsvdc_property_add_string(reply, "name",         "output");
  dsvdc_property_add_int(reply,    "defaultGroup", 1);        // light?
  dsvdc_property_add_uint(reply,    "function",     0);       // 0: on off only
  dsvdc_property_add_uint(reply,    "outputUsage",  0);       // 0: undefined
  dsvdc_property_add_bool(reply,   "variableRamp", false);    // no ramp
  dsvdc_property_add_string(reply, "type",         "output"); // really needed?

  XLOG_DEBUG("VDSDDigOutput::handleGetOutputDescription");

  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetOutputSettings(dsvdc_property_t *property,const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    XLOG_WARNING("failed to allocate reply property for " << name);
    return;
  }

  dsvdc_property_t *nGroup;
  if (dsvdc_property_new(&nGroup) != DSVDC_OK) {
    XLOG_WARNING("failed to allocate reply property for " << name);
    dsvdc_property_free(reply);
    return;
  }

  dsvdc_property_add_uint(reply, "mode", m_mode);
  dsvdc_property_add_bool(reply, "pushChanges", false);

  XLOG_DEBUG("mode " << m_mode);
  
  // groups
  for (unsigned int i = 0; i < MAX_GROUPS; ++i) {
    if (m_groupMember[i]) {
      dsvdc_property_add_bool(nGroup, std::to_string(i).c_str(), true); // light  //TODO
    } else {
      if (m_group == i) {
        dsvdc_property_add_bool(nGroup, std::to_string(i).c_str(), true);
      }
    }
  }
  dsvdc_property_add_property(reply, "groups", &nGroup);

  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetChannelDescriptions(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    XLOG_WARNING("failed to allocate reply property for " << name);
    return;
  }

  dsvdc_property_t *nProp;
  if (dsvdc_property_new(&nProp) != DSVDC_OK) {
    XLOG_WARNING("failed to allocate reply property for " << name);
    dsvdc_property_free(reply);
    return;
  }
  dsvdc_property_add_uint(nProp,     "channelIndex", 0);
  dsvdc_property_add_double(nProp,   "max",          100.0);
  dsvdc_property_add_double(nProp,   "min",          0.0);
  dsvdc_property_add_string(nProp,   "name",         "brightness");
  dsvdc_property_add_double(nProp,   "resolution" ,  1.0);
  dsvdc_property_add_property(reply, "0",            &nProp);
  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetChannelSettings(dsvdc_property_t * /*property*/, const dsvdc_property_t * /*query*/, char* /*name*/)
{
  /* nop: no channel settings defined */
  XLOG_INFO("handleGetChannelSettings");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDDigOutput::handleGetChannelStates(dsvdc_property_t * property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    XLOG_WARNING("failed to allocate reply property for " << name);
    return;
  }

  dsvdc_property_t *nProp;
  if (dsvdc_property_new(&nProp) != DSVDC_OK) {
    XLOG_WARNING("failed to allocate reply property for " << name);
    dsvdc_property_free(reply);
    return;
  }
  //dsvdc_property_add_double(nProp, "age", 0);
  dsvdc_property_add_double(nProp, "value", m_tempValue);

  dsvdc_property_add_property(reply, "0", &nProp);
  dsvdc_property_add_property(property, name, &reply);
  XLOG_INFO("Request channel value: " << m_tempValue);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
uint8_t VDSDDigOutput::handleSetButtonInputSettings(dsvdc_property_t */*property*/, const dsvdc_property_t * properties, char* name)
{
  dsvdc_property_t *buttonInputProp;
  dsvdc_property_get_property_by_name(properties, name, &buttonInputProp);

  dsvdc_property_t *indexProperty;
  dsvdc_property_get_property_by_name(buttonInputProp, "0", &indexProperty);

  for (size_t i = 0; i < dsvdc_property_get_num_properties(indexProperty); i++) {
    char* propName = NULL;
    if (DSVDC_OK != dsvdc_property_get_name(indexProperty, i, &propName)) {
      continue;
    }
    if (0 == strcmp(propName, "function")) {
      dsvdc_property_get_uint(indexProperty, i, &m_function);
    }
    else if (0 == strcmp(propName, "group")) {
      dsvdc_property_get_uint(indexProperty, i, &m_group);
    }
  }
  notifyModelChange();
  return DSVDC_OK;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
uint8_t VDSDDigOutput::handleSetOutputSettings(dsvdc_property_t */*property*/, const dsvdc_property_t * properties, char* name)
{
  dsvdc_property_t *outputSettings;
  dsvdc_property_get_property_by_name(properties, name, &outputSettings);

  dsvdc_property_t *modeProperty;

  int retVal = dsvdc_property_get_property_by_name(outputSettings, "mode", &modeProperty);
  if (DSVDC_OK == retVal) {
    dsvdc_property_get_uint(outputSettings, 0, &m_mode);
  }

  dsvdc_property_t *groupsProperty;
  int groupIndx = 0;
  bool groupSet = false;
  retVal = dsvdc_property_get_property_by_name(outputSettings, "groups", &groupsProperty);
  if (DSVDC_OK == retVal) {
    for (size_t i = 0; i < dsvdc_property_get_num_properties(groupsProperty); i++) {

      char* propName = NULL;
      if (DSVDC_OK != dsvdc_property_get_name(groupsProperty, i, &propName)) {
        continue;
      }
      groupIndx = std::stoi(propName);

      if (groupIndx >= MAX_GROUPS) {
        continue;
      }

      if (DSVDC_OK != dsvdc_property_get_name(groupsProperty, i, &propName)) {
        continue;
      }

      if (DSVDC_OK == dsvdc_property_get_bool(groupsProperty, i, &groupSet))
      {
          m_groupMember[groupIndx] = groupSet;
      }
    }
  }
  XLOG_INFO("VDSDDigOutput::handleSetOutputSettings mode: " << m_mode 
           << ", group: " << groupIndx 
           << ", value: " << groupSet);
  notifyModelChange();
  return retVal;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
uint8_t VDSDDigOutput::handleSetName(dsvdc_property_t */*property*/, const dsvdc_property_t * properties, char* /*name*/)
{
  char* data;
  dsvdc_property_get_string(properties,0,&data);
  m_name = data;
  notifyModelChange();
  return DSVDC_OK;
}

/***********************************************************************//**
  @method :  emitValue
  @comment:  set value to dig output device
  @param  :  value
***************************************************************************/
void VDSDDigOutput::emitValue(double value)
{ 
  XLOG_DEBUG("VDSDDigOutput::emitValue: " << m_pin);

  // lazy creation
  if (not m_outputDevice) {
      m_outputDevice = std::make_unique<Driver::OutputDevice>(m_pin);
  }
  m_outputDevice->setValue((value < 50));
 }

/***********************************************************************//**
  @method :  handleDeviceIcon
  @comment:  handler for device icon
  @param  :  property fill data
  @param  :  name name of property
***************************************************************************/
void VDSDDigOutput::handleDeviceIcon(dsvdc_property_t *property, char* name)
{
  XLOG_DEBUG("VDSDDigOutput::handleDeviceIcon");
  dsvdc_property_add_bytes(property, name, m_icon, m_iconSize);
}

/***********************************************************************//**
  @method :  handleDeviceIconName
  @comment:  handler for device icon name
  @param  :  property fill data
  @param  :  name name of property
***************************************************************************/
void VDSDDigOutput::handleDeviceIconName(dsvdc_property_t *property, char* name)
{
  XLOG_DEBUG("VDSDDigOutput::handleDeviceIconName");
  dsvdc_property_add_string(property, name, m_iconName.c_str());
}
