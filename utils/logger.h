/***************************************************************************
                           logger.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LOGGER_H
#define LOGGER_H

#include <mutex>
#include <sstream>
#include <string>
#include <vector>

typedef enum {
  EMERG   = 0,  /* system is unusable */
  ALERT   = 1,  /* action must be taken immediately */
  CRIT    = 2,  /* critical conditions */
  ERR     = 3,  /* error conditions */
  WARNING = 4,  /* warning conditions */
  NOTICE  = 5,  /* normal but significant condition */
  INFO    = 6,  /* informational */
  DEBUG   = 7,  /* debug-level messages */
  LEVEL_MAX = DEBUG + 1
} LOG_LEVEL;

/***************************************************************************/
// defines for logging
#define DBG_X

#define XLOG_EMERG(str)  \
{ \
  Logger::getInstance().logMessage(EMERG,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define XLOG_ALERT(str)  \
{ \
  Logger::getInstance().logMessage(ALERT,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define XLOG_CRIT(str)  \
{ \
  Logger::getInstance().logMessage(CRIT,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define XLOG_ERR(str)  \
{ \
  Logger::getInstance().logMessage(ERR,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define XLOG_WARNING(str)  \
{ \
  Logger::getInstance().logMessage(WARNING,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#ifdef DBG_X

#define XLOG_NOTICE(str)  \
{ \
  Logger::getInstance().logMessage(NOTICE,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define XLOG_INFO(str)  \
{ \
  Logger::getInstance().logMessage(INFO,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#define XLOG_DEBUG(str)  \
{ \
  Logger::getInstance().logMessage(DEBUG,__FILE__,__LINE__, \
    dynamic_cast<std::ostringstream&>(std::ostringstream().flush() << str)); \
  };

#else
#define XLOG_NOTICE(str) {};
#define XLOG_INFO(str)  {};
#define XLOG_DEBUG(str)  {};
#endif

class Logger
{
public:
  static Logger& getInstance();
  void setLogLevel(LOG_LEVEL level);
  void logMessage(LOG_LEVEL level, std::string const & module, int32_t line, std::ostringstream& str);

private:
  Logger();
  LOG_LEVEL m_logLevel;
  std::mutex m_lock;
};

#endif // LOGGER_H
