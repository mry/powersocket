/***************************************************************************
                           threadinterface.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "threadinterface.h"

/***********************************************************************//**
  @method :  ThreadInterface
  @comment:  constructor
***************************************************************************/
ThreadInterface::ThreadInterface() 
: m_running(false)
{
}

/***********************************************************************//**
  @method :  ~ThreadInterface
  @comment:  destructor
***************************************************************************/
ThreadInterface::~ThreadInterface()
{
}

/***********************************************************************//**
  @method :  startThread
  @comment:  start the posix thread.
***************************************************************************/
void ThreadInterface::startThread()
{
  if (!m_running) {
    m_running = true;
    m_threadx = std::make_unique<std::thread>([&]() {run();});
  }
}

/***********************************************************************//**
  @method :  stopThread
  @comment:  stop the posix thread.
***************************************************************************/
void ThreadInterface::stopThread()
{
  if (m_running) {
    m_running = false;
    m_threadx->join();
    m_threadx.reset();
  }
}
