#ifndef INTERFACEHELPER_H
#define INTERFACEHELPER_H


#include  <cstddef>

namespace InterfaceHelper
{
void GetPrimaryIp(char* buffer, size_t buflen);
};

#endif // INTERFACEHELPER_H
