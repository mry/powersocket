/***************************************************************************
                           logger.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <ctype.h>
#include <sstream>      // std::ostringstream
#include <iostream>     // std::cout, std::ios
#include <sstream>      // std::ostringstream
#include <string.h>
#include <stdarg.h>
#include <syslog.h>
#include <vector>
#include <sys/time.h>

// own
#include "utils/logger.h"

namespace   // anonymous namespace
{

/***********************************************************************//**
  @method : trim
  @comment: trim a file. remote \t \n \r from end
  @param  :  _str input string
  @return : trimmed string
***************************************************************************/
std::string trim(const std::string& _str) 
{
  std::string result = _str;
  std::string::size_type notwhite = result.find_first_not_of( " \t\n\r" );
  result.erase(0,notwhite);
  notwhite = result.find_last_not_of( " \t\n\r" );
  result.erase( notwhite + 1 );
  return result;
} // trim

/***********************************************************************/ /**
  @method : splitString
  @comment: split a input string at given delimiter signs and trim, if needded
  @param  : source input string
  @param  : delimiter delimiting character to split the string
  @param  : trimEntries trim the string
  @return : vector of split strings
***************************************************************************/
std::vector<std::string> splitString(const std::string &source,
                                     const char delimiter,
                                     bool trimEntries)
{
  std::vector<std::string> result;
  std::string curString = source;
  std::string::size_type delimPos = 0, startField = 0, skip = 0;

  while (startField < curString.size()) {
    delimPos = curString.find(delimiter, skip);

    if ((delimPos > 0) && (delimPos != std::string::npos)) {
      if (curString.at(delimPos - 1) == '\\') {
        // remove escape character
        curString.erase(delimPos - 1, 1);
        skip = delimPos;
        continue;
      }
    }

    if (delimPos != std::string::npos) {
      if (trimEntries) {
        result.push_back(trim(curString.substr(startField, delimPos - startField)));
      } else {
        result.push_back(curString.substr(startField, delimPos - startField));
      }
      skip = startField = delimPos + 1;
      if (curString.size() - skip == 0) {
        // in case of trailing delimiter, but no actual field data, we add an
        // empty string. not sure why we need this, probably nobody does
        result.push_back("");
      }
    } else {
      if (trimEntries) {
        result.push_back(trim(curString.substr(startField)));
      } else {
        result.push_back(curString.substr(startField));
      }
      break;
    }
  }
  return result;
} // splitString
}


/***********************************************************************/ /**
  @method :  Logger
  @comment:  constructor
***************************************************************************/
Logger::Logger()
: m_logLevel(NOTICE)
{
}

/***********************************************************************/ /**
  @method :  getInstance
  @comment:  get singleton instance
***************************************************************************/
Logger &Logger::getInstance()
{
  static Logger m_log;
  return m_log;
}

/***********************************************************************/ /**
  @method :  setLogLevel
  @comment:  set the log level. See also LOG_LEVEL.
  @param  :  level log level to set
***************************************************************************/
void Logger::setLogLevel(LOG_LEVEL level)
{
  m_logLevel = level;
}

/***********************************************************************/ /**
  @method :  logMessage
  @comment:  log a  message
  @param  :  level log level
  @param  :  module name
  @param  :  line line in module
  @param  :  str output string message 
***************************************************************************/
void Logger::logMessage(LOG_LEVEL level, 
                   std::string const &module, 
                   int32_t line, 
                   std::ostringstream &str)
{
  if (level <= m_logLevel) {
    using namespace std;

    unique_lock<std::mutex> lock(m_lock);
    ostringstream output;

    // Timestamp
    char buf2[BUFSIZ + 100];
    char tsbuf[30];
    
    struct timeval t;
    gettimeofday(&t, NULL);
    strftime(tsbuf, sizeof(tsbuf), "%Y-%m-%d %H:%M:%S", localtime(&t.tv_sec));
    (void)snprintf(buf2, BUFSIZ, "[%s.%03d]", tsbuf, (int)t.tv_usec / 1000);
    output << buf2;

    int32_t logLevel = DEBUG;
    switch (level) {
      case EMERG:   output << " ERR: "; break;
      case ALERT:   output << " ALT: "; break;
      case CRIT:    output << " CRT: "; break;
      case ERR:     output << " ERR: "; break;
      case WARNING: output << " WRN: "; break;
      case NOTICE:  output << " NTC: "; break;
      case INFO:    output << " INF: ";  break;
      case DEBUG:   output << " DBG: "; break;
      default: output << " NOP: ";
    }

    std::vector<std::string> vec = splitString(module, '/', true);
    if (vec.size() > 1) {
      std::string out = vec[vec.size() - 1];
      output << out << ":" << line << "->" << str.str();
    } else {
      output << module << ":" << line << "->" << str.str();
    }

    const string &tmp = output.str();
    const char *cstr = tmp.c_str();
    ::syslog(logLevel, "%s", cstr);

    cout << output.str() << endl;
  }
}


