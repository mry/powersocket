/***************************************************************************
                           threadinterface.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef THREADINTERFACE_H
#define THREADINTERFACE_H

#include <memory>
#include <thread>

class ThreadInterface
{

public:
  ThreadInterface();
  virtual ~ThreadInterface();
  virtual void startThread();
  virtual void stopThread();
  
  bool isRunning() const {return m_running;}

protected:

  /***********************************************************************//**
    @method : run
    @comment: main threading operation.
              Has to be implemented in subclass
  ***************************************************************************/
  virtual void run() = 0;

private:
  bool m_running;
  std::unique_ptr<std::thread> m_threadx;
};

#endif // THREADINTERFACE_H
