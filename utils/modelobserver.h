/***************************************************************************
                           modelobserver.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MODELOBSERVER_H
#define MODELOBSERVER_H

class ModelObserver
{
public:
  ModelObserver() = default;
  virtual ~ModelObserver() = default;

/***********************************************************************//**
  @method :  Update
  @comment:  called from modelsubject to
             inform observer of a change in model
***************************************************************************/
  virtual void Update() = 0;
};

#endif // MODELOBSERVER_H
