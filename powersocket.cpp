/***************************************************************************
                           powersocket.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "utils/logger.h"
#include "factory.h"

#include <unistd.h>
#include <signal.h>

int gotSignal = 0;

/***********************************************************************//**
  @method :  onSignal
  @comment:  Get the signal and save for main-loop processing
  @param  :  signo
***************************************************************************/
void onsignal(int signo)
{
    gotSignal = signo;
}

/***********************************************************************//**
  @method :  print_usage
  @comment:  display startup parameters
***************************************************************************/
void print_usage()
{
  fprintf(stderr, "Usage: powersocket [OPTION] ");
  fprintf(stderr, "TODO:");
  fprintf(stderr, " -h      --  print this help");
  fprintf(stderr, " -d <n>  --  set debug level  n= [1..7] ");
  fprintf(stderr, " -f <x>  --  filename of persistent data");
  fprintf(stderr, " -M <x>  --  directory of data (persistent and icon)");
}

/***********************************************************************//**
  @method :  main
  @comment:  main function
***************************************************************************/
int main(int argc, char* argv[])
{
  if (argc < 2) {
    print_usage();
    exit(-1);
  }

  //std::string serialport("/dev/ttyUSB0");
  std::string dataDirectory(".");
  std::string persistentFile("data.bin");
  LOG_LEVEL debugLevel = DEBUG;

  /* Look for options */
  #define CFG_OPT "c:"

  int c;
  while ((c = getopt(argc, argv, "hd:f:M:" CFG_OPT)) != -1) {
    switch (c) {
    case 'h':
      print_usage();
      exit(0);
    case 'd':
      debugLevel = static_cast<LOG_LEVEL> (atoi(optarg));
      break;
    //case 'p':
    //  serialport = optarg;
    //  break;
    case 'f':
      persistentFile = optarg;
      break;
    case 'M':
      dataDirectory = optarg;
      break;
    }
  }

  Logger::getInstance().setLogLevel(debugLevel);

  Factory factory(persistentFile, dataDirectory);
  factory.startup();

  /* Trap signals */
  gotSignal = 0;
  signal(SIGINT,  onsignal);
  signal(SIGTERM, onsignal);
  signal(SIGQUIT, onsignal);
  signal(SIGPIPE, SIG_IGN);
  signal(SIGUSR1, SIG_IGN);
  signal(SIGUSR2, SIG_IGN);

  while(not gotSignal) {
      sleep(2);
  }

  factory.shutdown();

  switch (gotSignal) {
    case SIGTERM:
    case SIGINT:
      XLOG_NOTICE("got SIGTERM or SIGINT");
    break;
    default:
      XLOG_NOTICE("got signal" << gotSignal << " -- ignored");
    break;
  }

  return 1; // Could be return(0)
}
