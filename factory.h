/***************************************************************************
                           factory.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef FACTORY_H
#define FACTORY_H

#include "mainthread.h"
#include "webserver/webserver.h"

#include "utils/modelsubject.h"

#include "utils/persistencyitf.h"
#include "utils/deviceadministrationitf.h"

class Factory 
: public PersistencyItf
, public DeviceAdministrationItf
{
public:
  Factory(std::string& dataFile, std::string& dataDirectory);
  ~Factory();
  void startup();
  void shutdown();

  void createDigOutputDevice(int32_t pin) override;

  std::vector<boost::shared_ptr<IVDSD> > getDevices() override;

  void saveData() override;

private:
  void loadData();
  void checkCreateVdc();
  void checkCreateDigOutput();

private:
  bool m_bStartedUp;
  std::shared_ptr <MainThread> m_mainThread;
  std::shared_ptr<Webserver> m_webserver;

  uint8_t* m_memblock;
  std::streampos m_size;

  std::string m_serialPort;
  std::string m_persistentFile;
  std::string m_dataDirectory;

  ModelSubject m_updateSubject;

  std::mutex  m_CreationMutex;

};

#endif // FACTORY_H
